FROM continuumio/miniconda3
MAINTAINER HAL24K "docker@hal24k.com"

# Create a working directory
RUN mkdir water_social_sentiment
WORKDIR water_social_sentiment

# Setup HAL24K pip server creds
ARG PIP_TRUSTED_HOST=pipserver.dimension.ws
ARG PIP_EXTRA_INDEX_URL=https://hal_muser:Mkw5VxNDNxkOMYrIDqdAy645mLGvnqxW@pip.dimension.ws:443/

# Copy install dependencies and create conda environmemnt
COPY environment.yml .

RUN conda env create -f environment.yml
RUN echo "source activate app_env" > ~/.bashrc \
    && conda clean -afy
ENV PATH /opt/conda/envs/app_env/bin:$PATH

# Copy app files to image REDUNANT AS BELOW 
#COPY ./src ./src
#WORKDIR src
#ENV WRKDIR /water_social_sentiment/src


# Copy app files to image
COPY ./src .
#ENV PYTHONPATH=/src:/src/water-social-sentiment
#ENV SERVER_HOST='127.0.0.1' 
#ENV SERVER_PORT=8050
#EXPOSE 8050

#ENV URL_PREFIX='/'
ENV PYTHONPATH=/app:/app/water-social-sentiment
ENV SERVER_HOST='0.0.0.0'
#ENV SERVER_HOST='127.0.0.1'
ENV SERVER_PORT=8000
EXPOSE 8000

RUN ls -lcrth && which python && echo $PYTHON_PATH

#CMD [ "python", "app.py"]


#CMD [ "gunicorn", "-b 0.0.0.0:80", "--timeout=600", "app:server"]
CMD [ "gunicorn", "-b 0.0.0.0:80", "--timeout=600", "app:run_app"]
import dash_table


import pandas as pd
import dash_html_components as html
from html_dash_templates import HtmlNavTemplates

#from app_data import AppData 


#dfaction = AppData().get_actionable_insight()

class HtmlTableTemplates():

   

    def __init__(self, df = pd.DataFrame() ):
         self.df = df 
         pass
        
    def get_basic_table(self, title, id_ = 'table'):
        """get basic html table - palceholder atm"""
        return  html.Div(className = 'actionable-insights-table', 
                        children = [html.H4(f"{title}"), 
                            dash_table.DataTable(
                                id= id_,
                                columns=[{"name": i, "id": i} for i in self.df.columns],
                                data= self.df.to_dict('records'),
                                style_data_conditional=[
                                                        {
                                                            'if': {'row_index': 'odd'},
                                                            'backgroundColor': 'rgb(248, 248, 248)'
                                                        }], 
                                )])

    def get_clickable_table(self, title, df, range_):
        """get basic html table - palceholder atm"""
        min_ = range_[0]
        max_ = range_[1]

        def make_datapoints_greater_than(df,  length = 10):
            """for a data set check the length is greater than define if not add ampty rows"""
            if len(df) > length:
                #print("df dict ", df.to_dict('records'))
                #print()
                return df.to_dict('records')
            else:
                #print("TOO SHORT making up blank lines")
                #print(df.to_dict('records'))
                # need to add n empyt {'Priority': 'High', 'Node ID': '16753', 'Location': 'wesex', 'Probability': 21, 'When': -4},
                # where n is the number to make 10 
                dict_ = df.to_dict('records')
                missing = length - len(df.to_dict('records'))
                dict_empty_rows = [{'Priority': '', 'Node ID': '', 'Location': '', 'Probability': '', 'When': ''} for i in range(0, missing)]
                #print("EMPTY DICT , " , dict_empty_rows)
                dict_.extend(dict_empty_rows )
                #print("EMPTY DICT + dict , " , dict_)
                return dict_


        try: 
            df = df.loc[(df["Probability"] > min_ ) &
                        (df["Probability"] < max_ )
                        ]
            return  html.Div(className = 'actionable-insights-table', 
                        children = [html.H4(f"{title}"), 
                            dash_table.DataTable(
                                id='datatable-row-ids',
                                #id='id-actionable-insights-table',
                                columns=[{"name": i, "id": i} for i in df.columns],
                                data= make_datapoints_greater_than(df = df),
                                editable=True,
                                filter_action="native",
                                sort_action="native",
                                sort_mode='multi',
                                row_selectable='multi',
                                #row_deletable=True,
                                selected_rows=[],
                                page_action='native',
                                page_current= 0,
                                page_size= 10,
                                style_data_conditional=[
                                                        {
                                                            'if': {'row_index': 'odd'},
                                                            'backgroundColor': 'rgb(248, 248, 248)'
                                                        }], 
                                ), 
                                 
                                html.Div(HtmlNavTemplates().get_slider_type(slidertype = 'percent-range'), 
                                            style = {'padding' : '100px'}
                                ), 

                                html.Div(id='updatemode-output-container', #style={'margin-top': 20}
                                ),
                                
                                ])
    
        except Exception as e:
            print(e)
        


import dash_html_components as html
import dash_core_components as dcc

import importlib

spec_ = importlib.util.find_spec("dash_daq")
found_ = spec_ is not None
if found_: 
    import dash_daq as daq
else:
    print("dash_daq, functions with this dependency wont work")




from datetime import datetime, date


import base64

image_logo = r"assets/collaborate-dimension.png" 
image_logo_encoded = base64.b64encode(open(image_logo , 'rb').read()).decode('ascii')

image_logo2 = r"assets/read-the-docs-logo-dark.png"
image_logo2_encoded = base64.b64encode(open(image_logo2 , 'rb').read()).decode('ascii')





class HtmlNavTemplates():

    def __init__(self):
        pass

    def percentage_slider(self):
        """get slider with percentage change for nan header"""
        return dcc.Slider(
                                min=0,
                                max=100,
                                value=5,
                                marks={
                                    0: {'label':  '25 Risk %', 'style': {'color': '#66FF00'}},
                                    50: {'label': '50 Risk %'},
                                    75: {'label':  '75 Risk %'},
                                    100: {'label':  '100 Risk %', 'style': {'color': '#FF4A00'}}
                                }
                            ) 
    
    def percentage_range_slider(self):
        """"get slider with percentage change range"""

        return html.Div([dcc.RangeSlider(
                            id='my-range-slider',
                            min=0,
                            max=100,
                            step=1, 
                            marks={
                                    0: {'label':  '0%', },  #'style': {'color': '#66FF00'}
                                    25: {'label':  '25%', }, 
                                    50: {'label': '50%'},
                                    75: {'label':  '75%'},
                                    100: {'label':  '100%', } #'style': {'color':  '#FF4A00'}
                                }
                        ),
                        #html.Div(id='output-container-range-slider')
                        ])




    def get_slider_type(self, slidertype):
        """gets a slider with various perused options, 
        for options on sliders see dash docs https://dash.plotly.com/dash-core-components/slider

        Args: 

        slidertype - string - specify the type currently 'percent', percent-range only 

        Retunrs:
        slider html or note tht slider is not supported
        
        """
        if slidertype == 'percent':
            return self.percentage_slider()
        elif slidertype == 'percent-range':
            return self.percentage_range_slider()
        
        else:
            return html.Div("slidertype not avaiable" )

    def get_options(self):

        return html.Div(className = 'options-grid', 
                        children = [
                            #html.Div(className = 'live-switch-background', 
                                    #children = 
                                        #[   daq.BooleanSwitch(
                                        #    id='nav-live-switch',
                                        #    className= "live-switch", #"nav__live-switch--icon",
                                        #    on=False,
                                        #    label="Live",
                                        #    labelPosition="right",
                                        #    #color="#5cb85c"
                                        #), ], 
                                    #), 
                            
                            html.Div(className = 'id-date-selector', 
                                    style = {'z-index' : 5000}, 
                                    children = [ 
                                        #html.Div("Select Dates"), 
                                        dcc.DatePickerRange(
                                                    id='id-nav-datepicker-range',
                                                    min_date_allowed=date(2019, 4, 5), 
                                                    max_date_allowed=date(2020, 3, 18), #datetime.now(),
                                                    initial_visible_month=date(2019, 4, 5),
                                                    start_date = date(2019, 12, 25), 
                                                    end_date=date(2020, 3, 18), 
                                                    display_format='DD-MM-YYYY',

                                                ),
                                                
                                         html.Div( style = {"width":"50%"} , 
                                                    children = [dcc.Dropdown(id='id-model-dropdown',
                                                        options=[
                                                            {'label': 'prophet', 'value': 'prophet_forecast'},
                                                            {'label': 'lstm', 'value': 'lstm'},
                                                            {'label': 'SARIMA', 'value': 'sarima'}
                                                        ],
                                                        value='prophet_forecast'
                                                    ),]), 

                                           

                                            

                                                                                    
                
                                            dcc.Store(id='nav-prev-interval', data=0),
                                            #dcc.Interval(
                                            #        id='nav-interval',
                                            #        interval= 300 * 1000,  # in milliseconds (300 seconds)
                                            #        n_intervals=0
                                            #),

                                            
                                            
                                            
                                            
                                            #]
                                            #),


                             #html.Div(children = [ #html.Div("Select Model"), 
                             ]), 

                            

                             html.Div(id = 'id-data-store-div' )
                                                   
                                

                            

                            #html.Div(className = 'set-selections', 
                            #        children = [
                            #            html.Button(
                            #                id="nav-set-bttn",
                            #                className="nav__set--bttn",
                            #                value="SET",
                            #                children=[
                            #                    html.P(
                            #                        "SET"), 
                            #                        ]
                            #                     ),
                                             #],)
                                    ]
                            
                            )
                        



    def slider_options_nav(self, slidertype = 'percent'):
        """generates a nav bar with the slider and options
        created for sewer blockage app """
        return html.Div(className = 'nav-container', 

                        children = [
                            html.Div(className = 'nav-logo', 
                                children = [ 
                                    #self.get_slider_type(slidertype = 'percent-range')
                                    html.Img(src='data:image/png;base64,{}'.format(image_logo_encoded)), 
                                    

                                    

                                    
                                ]), 


                            html.Div(className = 'nav-options', 
                                children = [self.get_options()]), 

                             html.Div(className = 'nav-logo2', 
                                children = [ 
                                    #self.get_slider_type(slidertype = 'percent-range')
                                    html.A( href = '../docs/_build/html/app.html', 
                                    children = [html.Img(src='data:image/png;base64,{}'.format(image_logo2_encoded))], 
                                    target='_blank', 
                                    )
                                
                                ]), 

                            
                            html.Div(className = 'nav-title', 
                                    id = 'id-nav-title', 
                                     
                                    #children = [html.H1("")], #"Combined Sewer Overflow - Anomaly Detetction"
                                    ), 
                            

                             ]
                        )

    def basic_options(self, dropdown_options = ('foo', 'bar'), dropdown_options2 = ('foo', 'bar') ):
            """generates a nav bar with the slider and options
            created for sewer blockage app """
            return html.Div(className = 'nav-container', 

                            children = [
                                html.Div(className = 'nav-logo', 
                                    children = [ 
                                        html.Img(src='data:image/png;base64,{}'.format(image_logo_encoded)), 
                              
                                    ]), 


                                html.Div(className = 'nav-options', 
                                    children = [

                                            html.Div( className = 'options-grid', # 'nav-options-content', #style = {"width":"50%"} , 

                                                    children = [

                                                        html.Div(className = 'drop-down-1', 
                                                            children = [
                                                            
                                                            html.Div("Select Company"), 

                                                            html.Div( style = {"width":"50%"}, 
                                                                children = [dcc.Dropdown(id='id-df-filter-dropdown',
                                                                    options=[ {'label': i, 'value': i} for i in dropdown_options],
                                                                    value = 'Bristol Water', 
                                                                    ),
                                                                    ]), 
                                                            
                                                            html.Div(id = 'id-selected_company'),
                                                        ]),  

                                                        html.Div( className = 'drop-down-2', 
                                                            children = [html.Div("Filter Results on Keywords"), 
                                                            html.Div(style = {"width":"50%"}, 
                                                                        children = [dcc.Dropdown(id='id-df-filter2-dropdown',
                                                                        options=[ {'label': i, 'value': i} for i in dropdown_options2],
                                                                        value = None ,
                                                                        multi = True,  
                                                                        ),])
                                                                        ]), 
                                                    
                                                    ]), 

                                    ]), 

                                html.Div(className = 'nav-logo2', 
                                    children = [ 
                                        #self.get_slider_type(slidertype = 'percent-range')
                                        html.A( href = '../docs/_build/html/app.html', 
                                        children = [html.Img(src='data:image/png;base64,{}'.format(image_logo2_encoded))], 
                                        target='_blank', 
                                        )
                                    
                                    ]), 

                                
                                html.Div(className = 'nav-title', 
                                        id = 'id-nav-title', 
                                        
                                        #children = [html.H1("")], #"Combined Sewer Overflow - Anomaly Detetction"
                                        ), 
                                

                                ]
                            )



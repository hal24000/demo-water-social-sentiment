import folium

class CollaboarteFoliumMaps():
    """Wrapper around Folium, 
        alternative class contructors for each company or river (to add) 
    """
    def __init__(self, 
                 location = [55.5, -3], 
                 zoom_start = 6, 
                 width = 1000, 
                 height = 1000):
        self.location = location
        self.zoom_start = zoom_start
        self.width = width
        self.height = height
        
        self.figure = folium.Figure(width= self.width, height= self.height)
        self.map = folium.Map(location=self.location,
                            tiles='OpenStreetMap',
                            zoom_start= self.zoom_start,
                            height = "100%",
                            width = "100%", 
                            #min_lat=- 90,
                            #max_lat=90,
                            #min_lon=- 180, 
                            #max_lon=180,
                            ).add_to(self.figure)
    
    def get_map(self):
        return self.map
    
    def add_polygons(self, polygon_filepath_dict):
        """"""
        for k, v in polygon_filepath_dict.items():
            folium.GeoJson(v, name=k).add_to(self.map)

    # above broken down and made generic ie add geojson / not tested yet 
    def add_geojson(self, polygon_filepath, name = "no name supplied"):
        folium.GeoJson(polygon_filepath, name=name).add_to(self.map)
        return print("Added geojson to map")
    def add_dict_of_geojsons(self, polygon_filepath_dict):
        """"""
        for k, v in polygon_filepath_dict.items():
            self.add_geojson(self, polygon_filepath = v, name = k)
        return print("Added geojsons dictionary to map")
    #### end of refcator - need to test ########
            
    def add_point(self, 
                  html_string = None, 
                  location = None, 
                  icon = None):
        
        if not html_string:
            html_string = '<H1> TEST/Default </H1>'
        if not location:
            location = self.location
            
        folium.Marker(location = location, 
                      popup= html_string, 
                     icon = icon, 
                     ).add_to(self.map)

    def plot_gdf_geometry_col(self, gdf, # note not tested on point col of multi -line -polygon
                          geo_col = 'geometry', 
                          popup_col =  'name',
                          color = 'red' ):  
        """"""
        for idx, row in gdf.iterrows():
            # lower the resolution to speed up plot
            sim_geo = gpd.GeoSeries(row[geo_col]).simplify(tolerance=0.001)
            # send to json & covert to foilum geojson object
            geo_j = sim_geo.to_json()
            geo_j = folium.GeoJson(data=geo_j,
                                style_function=lambda x: {'color': color }) # coloured red as alert
            # add popup to the geojson
            if popup_col: 
                folium.Popup(row[  popup_col ]).add_to(geo_j)
            else:
                print("pop up set to None")
            # add to map
            geo_j.add_to(self.map)
            
        return print("Added geometry column to map")
        
    def add_layer_control(self):
        """"""
        return folium.LayerControl().add_to(self.map)

    def get_raw_html(self):
        return self.map.get_root().render()
            
        
    
    @classmethod
    def BristolWater(cls):
        location = [51.38, -2.55]
        zoom_start = 10
        width = 750 
        height = 775 
        return cls(location  = location , 
                   zoom_start = zoom_start, 
                   width = width, 
                   height = height)
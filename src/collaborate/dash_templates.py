"""Main app file 
To do: move callbacks to seperate file to increase readability
"""

import dash
import logging
import json
import base64 



import dash_html_components as html
import dash_core_components as dcc
import dash_leaflet as dl
import dash_leaflet.express as dlx
import pandas as pd

from dash.dependencies import Input, Output, State
from datetime import date, datetime

# data pages 
from app_data import  conn

#plots
from dash_plotly_templates import  PlotlyTemplates

# dash pages 
from html_dash_templates import HtmlNavTemplates
from html_dash_table_templates import HtmlTableTemplates
from leaflet_dash_map_templates import  DashLeafletTemplates
from dash_model_evidence import ModelBreakdownTemplates

###### LOGS 


#######################

# get an actionable insights table from database 
dfaction = pd.read_csv("assets/dummyactionableinsights.csv")

# dash variables 
options =   # HtmlNavTemplates().slider_options_nav()
actionable_insightsdefault = HtmlTableTemplates().get_clickable_table(title = 'Blockage Detections', df = dfaction, range_ = [0, 100])
overview_visual = DashLeafletTemplates().get_area_map() #basic_map(title = 'Asset Loactions with Up/Down Stream Connectivity')
breakdown_plots = ModelBreakdownTemplates().get_sewerblockage_breakdown()




body = html.Div( className = 'grid-container', 
        children = [

                html.Div(className = 'options', 
                        children = [options]),           
                        
                html.Div(className = 'actionable-insights',
                        id = 'id-actionable-insights', 
                        children = [actionable_insightsdefault ], 
                        ),           
                        

                html.Div(className = 'overview-visual', 
                        children = [overview_visual ]),           
                        
                html.Div(className = 'model-rational', 
                        id = 'id-model-evidence-grid', 
                        children = [breakdown_plots], 
                        ),  

                 html.Div(className = 'footer', 
                        children = [ ]),           
                                      
            ])


app = dash.Dash(__name__, #external_stylesheets=external_stylesheets
                suppress_callback_exceptions=True, 
                    )



app.layout = html.Div( className = 'container', 
    children = [
            
                body, 
                dcc.Store(id='id-selected-station-store'),  # dcc.Store inside the app that stores the intermediate value
                dcc.Store(id='id-selected-date-store'), 
             ])


    


######################### CALLBACKS #######################################



######################### CALLBACKS - STORE LAST NODE #######################################
@app.callback([Output('id-selected-station-store', 'data'), 
                Output('id-data-store-div', 'children'), ], 
                [Input('datatable-row-ids', 'derived_virtual_row_ids'),
                Input('datatable-row-ids', 'selected_row_ids'),
                Input('datatable-row-ids', 'active_cell'), 
                #Input(component_id='demo-dropdown', component_property='value'), # removed a drop down 
                Input("geojson-nodes", "click_feature"), 
                Input('id-model-dropdown', 'value'),],
                State('datatable-row-ids', 'data'),  
                )
def store_selected_node(row_ids, selected_row_ids, active_cell, 
                        #input_value, # removed a dropdown 
                        map_click, 
                        model_name, 
                        state_data_table, 
                        
                        ):
        """Stores the node selected in the dahboard and stores to dcc.Store as json"""

        #print("SCTIVE CELL", active_cell)
        ctx = dash.callback_context # gets the last click
        default_station = '14002'
        selected_station = default_station
        print("MODEL SELECTED, " , model_name, type(model_name))

         

        #print(ctx)



        if not ctx.triggered:
                button_id = None
                selected_station = default_station

        else:
                button_id = ctx.triggered[0]['prop_id'].split('.')[0]
                
                if button_id == 'datatable-row-ids': # somehow this is triggered before it should be 
                        col = 'Node ID'
                        print("Table click triggered !!!!!")
                        if active_cell:
                                row = active_cell['row']
                                selected_station = state_data_table[row][col]
                                print("table clicked cellData", selected_station)
                elif button_id == 'geojson-nodes':
                        print(f"map clicked {map_click['properties']['node_id']}")
                        selected_station = map_click['properties']['node_id']
                elif button_id == 'No clicks yet':
                        selected_station = default_station
                else:
                        print("error")
                        selected_station = default_station
        
        print("CLICKED LAST", button_id)

        print("SELECTED STAION", selected_station)

        #model_name_test = 'foofar'


        response = json.loads(f'{{ "selected_station_STORED" : "{str(selected_station)}", "model_name" : "{model_name}" }}')
        print(response)
        print("FOO response", response["selected_station_STORED"])

        data_div = html.H2(f"Node: {selected_station}, Model: {model_name}")

        return response, data_div



######################### END STORE LAST NODE  #######################################


############### actionable insights ########################################



@app.callback(
    Output('id-actionable-insights', 'children'),
    [Input('my-range-slider', 'value')])
def update_output(value):
    """updates the actionable insights table"""
    print(f"value {value}")

    actionable_insights = HtmlTableTemplates().get_clickable_table(title = 'Blockage Detections', df = dfaction, range_ = value)
    
    return actionable_insights 

########################## Model Explanation CALLBACKS ################################

############################################################################


@app.callback( # figure
    Output(component_id='mainplot', component_property='figure'), 
    Input('id-selected-station-store', 'data'), 
    Input('id-selected-date-store', 'data')
    )
def update_evidence_div(selected_station, dates):
        """Updates the main evidence using the selected station """

        if selected_station:
                print("Selected Station store: ", selected_station['selected_station_STORED'])
                selected_station = selected_station["selected_station_STORED"]
        else:
                selected_station = "14002"
        #print("getting data for selected station, ", selected_station["selected_station_STORED"])



        cluster_nodes = conn.get_connected_nodes(node = selected_station)

        start_date = dates['start_date']
        end_date = dates['end_date']

        #testdf = conn.get_level_for_dates_for_cluster(parent = '14002', start = '02-05-2019', end = '02-02-2021')

        #print("CLUSTER NODES DICT ", cluster_nodes)

        #print("getting plot")

        print("START DATE, ", start_date)
        print("END DATE, ", end_date)

        df_levels =  conn.get_level_for_dates_for_cluster( parent = selected_station, start = start_date, end = end_date)

        try:
                df_levels['Datetime'] = pd.to_datetime(df_levels['Datetime'] )
        except Exception as e:
                print("EORRD ", e)

        print("Compare levels and predictions data")

        print(df_levels['Datetime'])
        print("dflevels min ", df_levels['Datetime'].min())
        print("dflevels max ", df_levels['Datetime'].max())
        print(len(df_levels['Datetime']))

        df_prediction = conn.get_model_forecast_for_node(node_id = selected_station , model = 'prophet_forecast')

        #print("Compare levels and predictions data - predictiosn ")

        #print(df_prediction['ds'])
        #print(df_prediction['ds'].min())
        #print(df_prediction['ds'].max())
        #print(len(df_prediction['ds']))

        fig = PlotlyTemplates().get_stacked_scatter(cluster_nodes = cluster_nodes, 
                                                        dflevels = df_levels, #conn.dflevels
                                                        dfrain = conn.get_rainfall_for_node(), 
                                                        df_prediction = df_prediction, 
                                                        )

        return fig

@app.callback(Output('my-range-slider', 'value'), [Input('my-range-slider', 'value')])
def test_click(value):
    """testing div - output to a testing div in bottom left"""
    if not value:
            value=[75, 100]
    else:
            value = value 
    print(f"new value : {value}")
    return value


########################## END Model Explanation CALLBACKS ################################

@app.callback(Output('updatemode-output-container', 'children'),
              Input('my-range-slider', 'value'))
def display_value(value):
        if value:
                return f"Slider set between {value[0]} and {value[1]} %"
        else:
                value = [75, 100]
                return f"Slider set between {value[0]} and {value[1]} %"


                


@app.callback(Output('id-selected-date-store', 'data'),
              Input('id-nav-datepicker-range', 'start_date'), 
              Input('id-nav-datepicker-range', 'end_date'))
def display_selected_date(start_date, end_date):
        print("FOOOOOOOOOOOOOOOOOOOOO")
        print("START DATE SELECTED ", start_date)
        print("END DATE SELECTED ", end_date)
        start_date = date.fromisoformat(start_date)
        end_date = date.fromisoformat(end_date)

        #d = datetime.strptime(start_date, '%Y-%m-%d')
        start_date = start_date.strftime('%m-%d-%Y')
        end_date = end_date.strftime('%m-%d-%Y')

        json_str = f'{{ "start_date" : "{(start_date)}" , "end_date" : "{end_date}" }}'
        print("JSON STRING", json_str )
        response = json.loads(json_str)

        print(response)
        return response




@app.callback(
    Output('id-nav-title', 'children'), 
    Input('id-selected-date-store', 'data'))
def update_output(data  ): #start_date, end_date
    string_prefix = 'You have selected: '
    if not data:
        text = html.H1("STORED DATA  Nothing slected")
    else:
        print("DATA ", data)
        #text = html.H1( f"STORED DATA {string_prefix}, {data}")
        text = f"{data['start_date']}"
    return text
        

    
    #if start_date is not None:
    #    start_date_object = date.fromisoformat(start_date)
    #    print ("Start Date Object ", str(start_date_object))
    #    start_date_string = start_date_object.strftime('%B %d, %Y')
    #    string_prefix = string_prefix + 'Start Date: ' + start_date_string + ' | '
    #if end_date is not None:
    #    end_date_object = date.fromisoformat(end_date)
    #    end_date_string = end_date_object.strftime('%B %d, %Y')
    #    string_prefix = string_prefix + 'End Date: ' + end_date_string
    #if len(string_prefix) == len('You have selected: '):
    #    return 'Select a date to see it displayed here'
    #else:
    #    print("DATES SELECTED, ", string_prefix )
        
    #    return string_prefix

############################################################################

def logs():
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    logging.info('Started')
    app.run_server(debug=True,
                )
    logging.info('Finished')

if __name__ == '__main__':
        logs()
        





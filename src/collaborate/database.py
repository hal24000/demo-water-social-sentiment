import os
import glob
import pandas as pd

import pandas as pd

from pymongo import MongoClient


class CollaborateDatabaseSettings():
    DB_URI: str = "mongodb+srv://cd_metwessexcso:lOPfNqR6WBJ0RApHwS82VyS0j2h10YcJ@atlas-prod-pl-0.hldmf.mongodb.net/cd_metwessexcso?retryWrites=true&w=majority"
    DB_NAME: str = "cd_metwessexcso"

class CollaborateDatabaseReader(CollaborateDatabaseSettings):
    
    """Sub class containing functions to read data only"""
    
    def __init__(self):
        self.client = MongoClient(CollaborateDatabaseSettings.DB_URI)
        self.db = self.client[CollaborateDatabaseSettings.DB_NAME]
    
    def list_collection_names(self, filter_ = None):
        return self.db.list_collection_names(filter = filter_)
    
    def get_collection(self, collection_name):
        """Gets a collection as"""
        return self.db[collection_name].find()
    
    def get_collection_as_dataframe(self, collection_name):
        data = self.get_collection(collection_name = collection_name)
        df = pd.DataFrame(list(data)) 
        return df

class CollaborateDatabaseWriter(CollaborateDatabaseReader):
    
    """Sub class of read data with extra writing functions"""
    
    def write_dataframe_to_database(self, df, collection_name):
        db[collection_name].insert_many(df.to_dict('records'))
        return print(f"data frame written to {collection_name} ")
    

import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter


import importlib

textblob_spec = importlib.util.find_spec("textblob")
found_textblob = textblob_spec is not None
if found_textblob: 
    from textblob import TextBlob
else:
    print("textblob not found, function with this dependency wont work")

wordcloud_spec = importlib.util.find_spec("wordcloud")
found_wordcloud = wordcloud_spec is not None
if found_wordcloud: 
    from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
else:
    print("wordcloud not found, function with this dependency wont work")






class CollaborateNLP():
    
    @staticmethod
    def get_dataframe_lower(df, cols = None):
        if not cols:
            for i in list(df):
                try:
                    df[i] = df[i].str.lower()
                except:
                    print(f"Skipping column {i} as not string ")
                    pass
            return df 
        else:
            for i in cols:
                try:
                    df[i] = df[i].str.lower()
                except:
                    print(f"Skipping column {i} as not string ")
                    pass
            return df 
            
    
    @staticmethod 
    def get_text_sentiment(df, col = 'text', identifier = None):
        if identifier:
            df[f'sentiment-{identifier}'] = df[col].apply(lambda tweet: TextBlob(tweet).sentiment)
            df[[f'polarity-{identifier}', f'subjectivity-{identifier}']] = pd.DataFrame(df[f'sentiment-{identifier}'].tolist(), index= df.index)
        else:
            df['sentiment'] = df[col].apply(lambda tweet: TextBlob(tweet).sentiment)
            df[['polarity', 'subjectivity']] = pd.DataFrame(df['sentiment'].tolist(), index= df.index)
        return df 
    
    @staticmethod
    def get_word_frequency(df, keywords : list = None, textcol = 'text'):
        # make a word counter from the list couting all words in all tweets 
        if not keywords:
            print("NO KEY WORDS SUPPLIED USING DEAFULT")
            keywords = ["chlorine", 
                 #"water", 
                 'contaminated', 
                 "bottled", 
                 "cloudy", 
                 "safe", 
                 "leaking", 
                 "sewage", 
                 "sewer",
                 "drain", 
                 "blocked", 
                 "drain", 
                 "drains", 
                 "smell", 
                 "taste", 
                 "foul", 
                 "tap", 
                 "fresh", 
                 "fine", 
                 "fined", 
                 "Road", 
                 "Street", 
                 "lane", 
                 "field", 
                 "town", 
                 "flooded", 
                 "flood", 
                 "overflow",
                 "complain", 
                 "complaint"
                 "blocked", 
                 "overflow", 
                 "manhole", 
                ]

            
        list_freq = []
        text = list(df[textcol])
        for i in text:
            #print(len(i))
            #print(i)
            text =  i.split(' ')
            for j in text:
                #'range(0, len(text)):
                #print(j)
                list_freq.append(j)
                
        d = Counter(list_freq )

        keyword_count_dict = {}
        # identify key words


        #parse counter dict and search for key words 
        for k, v in dict(d).items():

            #print(v)
            #print(k)
            for i in keywords:
                if i == k:
                    print(f"found {i} counted {v} times")
                    keyword_count_dict[i] = v
                else:
                    pass
                    #print(f"word {i} not found")
        return keyword_count_dict
    
    @staticmethod
    def my_tf_color_func(dictionary):
        def my_tf_color_func_inner(word, font_size, position, orientation, random_state=None, **kwargs):
            return "hsl(%d, 80%%, 50%%)" % (360 * dictionary[word])
        return my_tf_color_func_inner
    
    @staticmethod
    def get_frequency_word_cloud(keyword_frequency: dict):

        wordcloud = WordCloud(max_font_size=50, 
                              max_words=100, 
                              width = 500, 
                              height = 500, 
                              background_color="white", 
                              color_func= lambda *args, **kwargs: (255, 102, 0), #my_tf_color_func(newd), #lambda *args, **kwargs: (255, 102, 0), 
                             ).fit_words(frequencies = keyword_frequency) #generate(text)






        # Display the generated image:
        plt.figure(figsize=(10,10), 
                   #facecolor="#FFB380", 
            #edgecolor="#ff6600", 
                  )# ,  , 
        plt.imshow(wordcloud, interpolation='bilinear',   ) #cmap=rvb ,
        #plt.colorscale("#ff6600",  "#FFB380", "#000000" )
        #plt.colorbar();
        plt.axis("off")
        plt.show()
        return plt
                    

"""Main app file 
To do: move callbacks to seperate file to increase readability
"""

import dash
import logging
import json
import base64 



import dash_html_components as html
import dash_core_components as dcc
import dash_leaflet as dl
import dash_leaflet.express as dlx
import pandas as pd

from dash.dependencies import Input, Output, State
from datetime import date, datetime

# data pages 
#from app_data import  conn

#plots
#from dash_plotly_templates import  PlotlyTemplates

# dash pages 
from html_dash_templates import HtmlNavTemplates
from html_dash_table_templates import HtmlTableTemplates
#from leaflet_dash_map_templates import  DashLeafletTemplates
#from dash_model_evidence import ModelBreakdownTemplates

###### LOGS 


#######################

# get an actionable insights table from database 
dfaction = pd.read_csv("assets/dummy-actionable-insights.csv")
water_companies = ["bristol water"]
# dash variables 
options =  HtmlNavTemplates().basic_options(water_companies) #html.Div("Make an options grid ") # 
actionable_insightsdefault = HtmlTableTemplates(df = dfaction).get_basic_table(title = 'Blockage Detections')
overview_visual = html.Div("Make overview visuals ") # DashLeafletTemplates().get_area_map() #basic_map(title = 'Asset Loactions with Up/Down Stream Connectivity')
breakdown_plots = html.Div("Show evidence for insights and overview ") # ModelBreakdownTemplates().get_sewerblockage_breakdown()




body = html.Div( className = 'grid-container', 
        children = [

                html.Div(className = 'options', 
                        children = [options]),           
                        
                html.Div(className = 'actionable-insights',
                        id = 'id-actionable-insights', 
                        children = [actionable_insightsdefault ], 
                        ),           
                        

                html.Div(className = 'overview-visual', 
                        children = [overview_visual ]),           
                        
                html.Div(className = 'model-rational', 
                        id = 'id-model-evidence-grid', 
                        children = [breakdown_plots], 
                        ),  

                 html.Div(className = 'footer', 
                        children = [ ]),           
                                      
            ])


app = dash.Dash(__name__, #external_stylesheets=external_stylesheets
                suppress_callback_exceptions=True, 
                    )



app.layout = html.Div( className = 'container', 
    children = [
            
                body, 
                dcc.Store(id='id-selected-station-store'),  # dcc.Store inside the app that stores the intermediate value
                dcc.Store(id='id-selected-date-store'), 
             ])


    


######################### CALLBACKS #######################################



def logs():
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    logging.info('Started')
    app.run_server(debug=True,
                )
    logging.info('Finished')

if __name__ == '__main__':
        logs()
        






import geopandas as gpd

class CollaborateGeoData():
    """A class for working with geo data"""
    
    @staticmethod
    def get_geodf(file_, crs = "epsg:4326" ):
        """Retunrns a geodataframe"""
        "EU_BASINS"
        gdf = gpd.read_file(file_, ) #crs={'init': 'epsg:4326'}
        crs_orginal = gdf.crs
        print(f"Original CRS: {crs_orginal}")            
        #if crs_orginal != crs:
        gdf = gpd.GeoDataFrame(gdf)
        if crs_orginal != crs:
            gdf = gdf.to_crs(crs)
            print(f"Source CRS changed from {crs_orginal} to {crs}")
        return gdf

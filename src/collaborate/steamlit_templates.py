import streamlit as st



class StreamLitTemplates():
    """Preset page setups for Streamlit Apps"""
    
    def __init__(self, page_title):
        self.favicon = "static/favicon.ico"
        self.page_title = page_title
        
        
    def setup_page(self, layout = "wide", initial_sidebar_state = "expanded" ):
        
        """returns a page based"""
        
        page = st.set_page_config(
                                page_title = self.page_title,
                                page_icon = self.favicon,
                                layout = layout,
                                initial_sidebar_state =  initial_sidebar_state,
                                )
        
        return page
    
    def hide_streamlit_header(self):
        """Removes the streamlit header containing burger/dropdown with options"""
        
        css = """<style>header {visibility: hidden;}</style>"""
        hide_streamlit_header = st.markdown(css, unsafe_allow_html=True)
        
        return hide_streamlit_header
    
    def hide_streamlit_footer(self):
        """Hides the preset streamlit footer"""
        
        css = """<style>footer {visibility: hidden;}</style>"""
        hide_streamlit_footer = st.markdown(css, unsafe_allow_html=True)
        
        print("WARNING you are removing the attributions - needs placing elsewhere")
        return hide_streamlit_footer
    
    def remove_preset_padding(self):
        """Removes the preset padding from the app"""
        
        css = f"""<style>.reportview-container .main .block-container{{padding-top: 0;}}</style>"""
        remove_padding = st.markdown(css, unsafe_allow_html=True)
        
        return remove_padding
    
    def hide_presets(self):
        """hides the preset styles in streamlit"""#
        
        hide_streamlit_header = self.hide_streamlit_header()
        hide_streamlit_footer = self.hide_streamlit_footer()
        remove_padding = self.remove_preset_padding()
        
        return hide_streamlit_header, hide_streamlit_footer, remove_padding
        

class SideBars():
    
    def side_bar_form_clustering(self):
        with st.sidebar.form(key="clustering_form"):
                    time_select = st.selectbox("Select time", ["60m", "15m"])
                    cluster_number = st.selectbox("Select clusters", [ 12, 8, 6, 4, 3])
                    metric_select = st.selectbox("Select metric", ["DTW","Pearson", "Spearman"])
                    default_cutoff = 0.90 if time_select == "60m" else 0.75
                    cutoff_select = st.number_input(
                        "Select metric cutoff", min_value=-1.0, max_value=1.0, value=default_cutoff
                    )
                    graph_select = st.selectbox("Select level view", ["Standard", "Differenced"])
                    submit_button = st.form_submit_button(label="Update")
        return time_select, cluster_number, metric_select, default_cutoff, cutoff_select, graph_select, submit_button
        
    def info_general(self, info_text):
        with st.sidebar: 
            
                st.info(
                        info_text
                    )
        return None
    
    def side_bar_frequency_location(self, locations = [ "foo", "far", "poo", "bar"], 
                                            frequency = ["24hrs", 
                                                                "12hrs", 
                                                                "6hrs", 
                                                                "60m", 
                                                                "30m" 
                                                                "15m", 
                                                                "10m", 
                                                                "5m", 
                                                                "1m", 
                                                                ], 
                                            thresholdB1 = [ 1, 1.5], 
                                            thresholdB2 = [ 1, 1.5 ], 
                                            thresholdB3 = [ 1, 1.5 ], 

                                            ):
        """Provides a side bar with frequency and location select"""
        with st.sidebar.form(key="clustering_form"):
                    select_location =  st.selectbox("Select site", locations) 
                    select_frequency = st.selectbox("Select time", frequency)
                    submit_button = st.form_submit_button(label="Update")

       

        return (select_location,  
            select_frequency, 
            submit_button )

    def side_bar_frequency_location_thresholds(self, locations = [ "foo", "far", "poo", "bar"], 
                                            frequency = ["24hrs", 
                                                                "12hrs", 
                                                                "6hrs", 
                                                                "60m", 
                                                                "30m" 
                                                                "15m", 
                                                                "10m", 
                                                                "5m", 
                                                                "1m", 
                                                                ], 
                                            thresholdB1 = [ 1, 1.5], 
                                            thresholdB2 = [ 1, 1.5 ], 
                                            thresholdB3 = [ 1, 1.5 ], 

                                            ):
        """Provides a side bar with frequency and location select"""
        with st.sidebar.form(key="clustering_form"):
                    select_location =  st.selectbox("Select site", locations) 
                    select_frequency = st.selectbox("Select time", frequency)
                    select_thresholdB1lower = st.slider("B1 Lower Thershold", 0, 100, key='B1lower')
                    select_thresholdB1upper = st.slider("B1 Upper Thershold", 0, 100, key='B1upper')
                    select_thresholdB1lower = st.slider("B2 Lower Thershold", 0, 100, key='B2lower')
                    select_thresholdB1upper = st.slider("B2 Upper Thershold", 0, 100, key='B2upper')
                    select_thresholdB1lower = st.slider("B3 Lower Thershold", 0, 100, key='B3lower')
                    select_thresholdB1upper = st.slider("B3 Upper Thershold", 0, 100, key='B3upper')
                    submit_button = st.form_submit_button(label="Update")

        return (select_location,  
            select_frequency, 
            select_thresholdB1lower, 
            select_thresholdB1upper, 
            select_thresholdB1lower, 
            select_thresholdB1upper, 
            select_thresholdB1lower, 
            select_thresholdB1upper, 
            submit_button )

    def side_bar_thresholds(self,  
                            thresholdB1 = [ 1, 1.5], 
                            thresholdB2 = [ 1, 1.5 ], 
                            thresholdB3 = [ 1, 1.5 ], 
                            ):
        """Provides a side bar with frequency and location select"""
        with st.sidebar.form(key="thresholds"):
                    select_thresholdB1lower = st.slider("B1 Lower Thershold", 0, 100, key='B1lower')
                    select_thresholdB1upper = st.slider("B1 Upper Thershold", 0, 100, key='B1upper')
                    select_thresholdB1lower = st.slider("B2 Lower Thershold", 0, 100, key='B2lower')
                    select_thresholdB1upper = st.slider("B2 Upper Thershold", 0, 100, key='B2upper')
                    select_thresholdB1lower = st.slider("B3 Lower Thershold", 0, 100, key='B3lower')
                    select_thresholdB1upper = st.slider("B3 Upper Thershold", 0, 100, key='B3upper')
                    submit_button = st.form_submit_button(label="Update")

        return (select_thresholdB1lower, 
            select_thresholdB1upper, 
            select_thresholdB1lower, 
            select_thresholdB1upper, 
            select_thresholdB1lower, 
            select_thresholdB1upper, 
            submit_button )
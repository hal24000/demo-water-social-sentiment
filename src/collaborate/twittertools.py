import json 
import pandas as pd 
import numpy as np
import plotly.express as px
import plotly.graph_objects as go 



from .nlptools import CollaborateNLP


import importlib

dash_spec = importlib.util.find_spec("dash_html_components")
found_dash = dash_spec is not None
if found_dash: 
    import dash_html_components as html
else:
    print("Dash HTML components not found, function with this dependency wont work")



class CollaboarteTwitter(CollaborateNLP):

    # list of possible street suffixes in data 
    # taken from https://en.wikipedia.org/wiki/Street_suffix#In_England
    street_suffixs = [  'Road',
                        'Street', 
                        'Way', 
                        'Avenue', 
                        'Drive', 
                        'Lane', 
                        'Grove', 
                        'Gardens', 
                        'Circus', 
                        'Crescent', 
                        'Bypass', 
                        'Close', 
                        'Square', 
                        'Hill', 
                        'Mews', 
                        'Vale', 
                        'Rise',
                        'Row', 
                        'Mead',
                        'Wharf', 
                        'End',
                        'Court',
                        'Cross',
                        'Side',
                        'View',
                        'Walk',
                        'Park',
                        'Meadow',
                        'Green',
                        'Quadrant',
                        'Gate',
                        'Gait',
                        'Wynd',
                        'Brow',# Cumbria
                        #'Brae',  # scotland to mean a hill
                    ]
    
    def gen_tweet_list(self, tweet_files):
        """Read Json Files of tweets and add to list"""
        tweets = [] #empty list to store tweets
        #print("SCOTT EDIT 3 tweet files ", tweet_files )
        for i in tweet_files: #iterate through each file 
            print("SCOTT EDIT 3 tweet file ", i )
            search_terms = i.split("/")
           
            print("SCOTT EDIT 3 search_terms", search_terms )
            print("spliting of file path", search_terms) # if errors its likley due to split above i.e / vs \
            search_terms = search_terms[-3].replace("@", "")
            with open (i, 'r') as f: #open file with context manager with
                for line in f.readlines(): #read lines
                    line = json.loads(line)
                    line["search_terms"] = search_terms
                    tweets.append(line) #append each line to tweet list
                    # make a dict so that we can filter by water company 
                    #print(line)
        return tweets

    
    def build_tweet_df(self, tweet_files, df = None):
        """Creates a df from list of raw json files"""
        print("SCOTT EDIT 1: tweet files: ", tweet_files)
        if df is None: # check does this accept an existing df? i dont think so 
            df = pd.DataFrame()
            
        tweet_list = self.gen_tweet_list(tweet_files)
        #print("SCOTT EDIT 4, ", tweet_list[0])
            
        def get_df_info(field_name, id_1, id_2):
            """gets data from tweet list"""
            
            df[field_name] = list(map(lambda tweet: tweet[id_1][id_2]
                             if tweet[id_1] != None else np.nan,                                     
                                      tweet_list))
            
            
        get_df_info(field_name ='user_name', id_1='user', id_2='name')
        get_df_info(field_name ='screen_name_url', id_1='user', id_2='screen_name')
        get_df_info(field_name ='location', id_1='user', id_2='location')
        get_df_info(field_name ='time_zone', id_1='user', id_2='time_zone')
        get_df_info(field_name ='UTC', id_1='user', id_2='utc_offset')
        get_df_info(field_name ='country', id_1='place', id_2='country_code')
        get_df_info(field_name = 'coordinates', id_1='coordinates', id_2='coordinates')
        get_df_info(field_name = 'UTC offset', id_1='user', id_2='utc_offset')
        #get_df_info(field_name = 'conversation_id', id_1='user', id_2='conversation_id')
        
        
        try:
            get_df_info(field_name = 'retweeted_status', id_1= 'retweeted_status', id_2='id')
        except Exception as e:
            print(e)
            pass
        
        df['tweet_text'] = list(map(lambda tweet: tweet['full_text'], tweet_list))#
        df['tweet_id'] = list(map(lambda tweet: tweet['id'], tweet_list))#
        df['retweet'] = df['tweet_text'].str.contains("RT")
        df['reply_to'] = list(map(lambda tweet: tweet['in_reply_to_status_id'], tweet_list))#
        df['reply_to'] = df['reply_to'].fillna(0)
        df['search_terms'] = list(map(lambda tweet: tweet['search_terms'], tweet_list))#
        #df['conversation_id'] = list(map(lambda tweet: tweet['conversation_id'], tweets))
        df['created_at'] = list(map(lambda tweet: tweet['created_at'], tweet_list))
        
        
        
        #get_df_info(field_name = 'reply_to', id_1="in_reply_to_status_id", id_2=None)
        
        
        #df['retweet'] = list(map(lambda tweet: tweet[]['retweeted_status'], tweets))#
        #df['retweet'] = list(map(lambda tweet: tweet['in_reply_to_status_id'], tweets))#
        
        
       
        #df['UTC_Hrs'] = round(df['UTC']*0.000277778, 2) #convert UTC from seconds to hours
        
        #[x.strip('[') for x in df.coordinates]
        #[x.strip(']') for x in df.coordinates]
        
        #df['lat'], df['longe'] = (df['coordinates'], df['coordinates'][1])
        df["created_at"] = pd.to_datetime(df["created_at"])
        df["day"] = df["created_at"].dt.day_name()
        df["date"] = df["created_at"].dt.day
        df["month"] = df["created_at"].dt.month_name()
        df["year"] = df["created_at"].dt.year
        #df.index.append("created_at")
        #df = df.set_index(["month", "date", "created_at", "tweet_id" ]).sort_index()

        print("SCOTT EDIT 4, ", df)
        return df 

    def get_tweets_with_location_info(self, df):
        # roads etc 
        # this could be absracted out but what is that abstraction? 
        # ie what can street_suffies replace 
        series_roads = pd.Series()
        roads = [i.lower() for i in CollaboarteTwitter.street_suffixs] #['road', 'street', 'lane', ]
        for i in roads:
            last_word = i
            series = df.loc[df['tweet_text'].str.contains(last_word)]['tweet_text'].str.extractall(fr"([a-zA-Z'-]+\s+{last_word})\b").unstack().apply(lambda x:' '.join(x.dropna()), axis=1)
            print(series)
            series_roads = series_roads.append(series)
        return series_roads

    

 

    
    @staticmethod
    def print_tweet_text_and_link(df):
        #try:
        #    df = df.reset_index()
        #except Exception as e:
        #    print(e)
        #    pass
            
        for i in range(0, len(df)):
            print(df["tweet_text"][i]) #.reset_index().iloc[0])
            print(f"https://twitter.com/{str(df['screen_name_url'][i])}/status/{int(df['tweet_id'][i])}" ) 
            if df["reply_to"][i]: #.reset_index().iloc[0]
                print("In reply to", int(df["reply_to"][i]) ) #.reset_index().iloc[0]
                print(f"https://twitter.com/{str(df['user_name'][i]).replace(' ', '_')}/status/{int(df['reply_to'][i])}" )
            # was it replied to?
            # what is the sentiment 
    
    @staticmethod
    def filter_df_by_company(company, df, col = "search_terms"):
        # NOTE REQUIRES LOWERCASE!!! 
        return df.loc[df[col].str.contains(company)].copy()

    @staticmethod
    def get_tweet_text_html(df):
        # work on not sure its good
        html_list = []
        for i in range(0, len(df)):
            tweet = df["tweet_text"][i] #.reset_index().iloc[0])
            tweet_link = f"https://twitter.com/{str(df['screen_name_url'][i])}/status/{int(df['tweet_id'][i])}"  
            if df["reply_to"][i]: #.reset_index().iloc[0]
                in_reply = f'In reply to: {int(df["reply_to"][i])}' #.reset_index().iloc[0]
                #print(f"https://twitter.com/{str(df['user_name'][i]).replace(' ', '_')}/status/{int(df['reply_to'][i])}" )
            html_str = f'<div> { tweet } </div> <a href = {tweet_link}> link </a>'
            html_list.append(html_str)
        html_str = '</br>'.join([ i for i in html_list])

        return html_str

    @staticmethod
    def get_tweet_text_dash_html(df):
        if found_dash: 
            tweet_list = []
            link_list = []
            for i in range(0, len(df)):
                tweet = df["tweet_text"][i] #.reset_index().iloc[0])
                tweet_list.append(tweet)
                tweet_link = f"https://twitter.com/{str(df['screen_name_url'][i])}/status/{int(df['tweet_id'][i])}"
                link_list.append(tweet_link)
            #print("twet links" , link_list )

            return [ html.Div(className = 'tweet-text', 
                            children = [html.Div(i) , html.A( "link", href = j, target = '_blank') ])for i, j in zip(tweet_list, link_list)]
        else: 
            pass


                
    

class TweetPlots():
    """Class holding tweet plots for twitter data - likley move to a gebraic class"""
    def get_figure_(self, df, x ='days', timespan = 'No timespan supplied give at func call '):
        # Here we use a column with categorical data
        fig = px.histogram(df, 
                           x= x, 
                           title = f'tweet count last {timespan}', 
                           category_orders={"day": ["Monday", "Tuesday", "Wednesday", 
                                                        "Thursday", "Friday", "Saturday", "Sunday"]} )

        fig.update_layout(
                            autosize=False,
                            width=800,
                            height=600,
                            )

        return fig
    
    def plot_polarity_histogram_by_day(self, df, 
                                       timespan = 'No timespan supplied give at func call ', 
                                       x = "day", y = "polarity-raw" , histfunc="avg"):
    
        fig = px.histogram(df, x=x, y=y, histfunc=histfunc, title=f"Sentiment by day over {timespan}", 
                           category_orders={"day": ["Monday", "Tuesday", "Wednesday", 
                                                    "Thursday", "Friday", "Saturday", "Sunday"],}

                          )
        fig.update_traces(xbins_size="M1")
        fig.update_xaxes(showgrid=True, ticklabelmode="period", dtick="M1", tickformat="%b\n%Y")
        fig.update_layout(bargap=0.1)
        fig.add_trace(go.Scatter(mode="markers", x=df["day"], y=df["polarity-raw"], name="actual scores"))
        fig.update_layout(
                            autosize=False,
                            width=800,
                            height=600,
                            )

        return fig#.show()
    
    def plot_sentiment_pie(self, df, sentiment_measure = 'polarity-raw',  
                      labels = ['Highly Negative', 'Negative','Neutral', 'Positive', 'Highly Positive' ], 
                      timeperiod = 'unknown supply to fucntion call', 
                      ):
        df_ = df.copy() 
        bins = [-1, -0.50, -0.25, 0, 0.25, 0.5, 1]
        df_['binned'] = pd.cut(df_[sentiment_measure], bins)
        df_.groupby('binned').count()
        colors = ['darkred', 'red', 'orange', 'lightgreen', 'green']

        fig = go.Figure(data=[go.Pie(labels=labels, 
                                 values= df_.groupby('binned').count()['tweet_text'])])
        fig.update_traces(hoverinfo='label+percent', textinfo='value', textfont_size=20,
                      marker=dict(colors=colors, line=dict(color='#000000', width=2)))
        fig.update_layout(title = f'Sentiment Pie chart over {timeperiod}')
        fig.update_layout(
                            autosize=False,
                            width=800,
                            height=600,
                            )
        return fig
"""Main app file 
To do: move callbacks to seperate file to increase readability
"""

import dash
import logging
import json
import base64 



import dash_html_components as html
import dash_core_components as dcc
#import dash_leaflet as dl
#import dash_leaflet.express as dlx
import pandas as pd

from dash.dependencies import Input, Output, State
from datetime import date, datetime
from staticdata import df
from appstart import app

# data pages 
#from app_data import  conn

#plots
#from dash_plotly_templates import  PlotlyTemplates

# dash pages 
from html_dash_templates import HtmlNavTemplates
from html_dash_table_templates import HtmlTableTemplates
#from leaflet_dash_map_templates import  DashLeafletTemplates
#from dash_model_evidence import ModelBreakdownTemplates
from overview_visual import * #overview
from foliummap import folium_map
###### LOGS 
from staticdata import collabtwitter


#######################

# get an actionable insights table from database 
dfaction = pd.read_csv("assets/dummy-actionable-insights.csv")

water_companies = [ 'All', 
                'Anglian Water', 
                'Dŵr Cymru',
                'Welsh Water', 
                'Northumbrian Water',
                'Severn Trent Water', 
                'Southern Water', 
                'South West Water',
                'Thames Water', 
                'United Utilities', 
                'Wessex Water',
                'Yorkshire Water', 
                'Hafren Dyfrdwy', 
                'Affinity Water', 
                'Albion Water', 
                'Bournemouth Water',
                'Bristol Water', 
               'Cambridge Water Company',
               'Cholderton and District Water Company', 
               'Essex and Suffolk Water',
               'Hartlepool Water', 
               'Portsmouth Water', 
               'South East Water',
               'South Staffordshire Water', 
               'Sutton and East Surrey Water',
               'Youlgrave Waterworks',
                  ]

key_water_words = ['clean', 
                   'waste', 
                   'sewage', 
                   'dirty', 
                   'tap', 
                   'drain', 
                   'drain blocked', 
                   'overflow', 
                   'flouride', 
                  ]
# dash variables 
options =  HtmlNavTemplates().basic_options(dropdown_options  = water_companies, dropdown_options2  = key_water_words  ) #html.Div("Make an options grid ") # 
actionable_insights = HtmlTableTemplates(df = dfaction).get_basic_table(title = 'Actionable Insights')

#df_tweets = df[["user_name", "tweet_text", ]].copy()
tweets = collabtwitter.get_tweet_text_dash_html(df = df) #.loc[df['tweet_text'].str.contains('sewage')].reset_index())
#df_tweets["link"] =
#tweets_table = HtmlTableTemplates(df = df_tweets.head()).get_basic_table(title = 'Tweets', id_ = 'table2')

overview_visual = overview # html.Div("Make overview visuals ") # DashLeafletTemplates().get_area_map() #basic_map(title = 'Asset Loactions with Up/Down Stream Connectivity')
breakdown_plots = folium_map #html.Div("Show evidence for insights and overview ") # ModelBreakdownTemplates().get_sewerblockage_breakdown()

actionable_insights = [actionable_insights]
#actionable_insights.append( html.H4("Recent Tweets") )
#actionable_insights.extend(tweets)



body = html.Div( className = 'grid-container', 
        children = [

                html.Div(className = 'options', 
                        children = [options]),           
                        
                html.Div(className = 'actionable-insights',
                        id = 'id-actionable-insights', 
                        children = actionable_insights 
                        ),           
                        

                html.Div(className = 'overview-visual', 
                        children = [overview_visual ]),           
                        
                html.Div(className = 'model-rational', 
                        id = 'id-model-evidence-grid', 
                        children = [breakdown_plots], 
                        ),  

                 html.Div(className = 'footer', 
                        children = [ ]),           
                                      
            ])




app.layout = html.Div( className = 'container', 
    children = [
            
                body, 
                dcc.Store(id='id-selected-station-store'),  # dcc.Store inside the app that stores the intermediate value
                dcc.Store(id='id-selected-date-store'), 
             ])


    
####################### CALL BACK DATA ##################################


######################### CALLBACKS #######################################




########################### RUN with logs ###############################

def logs():
    logging.basicConfig(filename='app.log', level=logging.INFO)
    logging.info('Started')
    app.run_server(debug=False,
                 port = 8000, 
                )
    logging.info('Finished')

if __name__ == '__main__':
        logs()
        

        




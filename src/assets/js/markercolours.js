window.myNamespace = Object.assign({}, window.myNamespace, {  
    mySubNamespace: {  
        pointToLayerRed: function(feature, latlng, context) {  
            console.log(context) 
            return L.circleMarker(latlng, {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 5
            })  
        
            
        }  

        

        
    }  
});



window.RedLayer = Object.assign({}, window.RedLayer, {  
    SubRedLayer: {  
        pointToLayerRed: function(feature, latlng, context) {  
            console.log(context) 
            return L.circleMarker(latlng, {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 10
            })  
        
            
        }  

        

        
    }  
});



window.AmberLayer = Object.assign({}, window.AmberLayer, {  
    SubAmberLayer: {  
        pointToLayerAmber: function(feature, latlng, context) {  
            console.log(context) 
            return L.circleMarker(latlng, {
                color: 'yellow',
                fillColor: 'yellow',
                fillOpacity: 0.5,
                radius: 7
            })  
        
            
        }  

        

        
    }  
});


window.GreenLayer = Object.assign({}, window.GreenLayer , {  
    SubGreenLayer : {  
        pointToLayerGreen: function(feature, latlng, context) {  
            console.log(context) 
            return L.circleMarker(latlng, {
                color: 'green',
                fillColor: 'green',
                fillOpacity: 0.5,
                radius: 5
            })  
        
            
        }  

        

        
    }  
});

window.MarkerClusters = Object.assign({}, window.MarkerClusters , {  
    SubMarkerClusters : { 
        iconCreateFunctionCustom: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            console.log(cluster) 
            if (childCount < 2) {
                console.log("small") 
              c += 'small';
            } 
            else if (childCount < 5) {
              c += 'medium';
            } 
            else {
              c += 'large';
            }
           
            return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
             className: 'marker-cluster' + c, iconSize: new L.Point(10, 10) }
             )
            }      
    }  
});

window.MarkerClustersRed = Object.assign({}, window.MarkerClustersRed , {  
    SubMarkerClustersRed : { 
        iconCreateFunctionRed: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            
            console.log("red") 
            if (childCount < 2) {
                console.log("small") 
              c += 'small-red';
            } 
            else if (childCount < 5) {
              c += 'medium-red';
            } 
            else {
              c += 'large-red';
            }
           
            return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
             className: 'marker-cluster' + c, iconSize: new L.Point(50, 50) }
             )
            }      
    }  
});

window.MarkerClustersAmber = Object.assign({}, window.MarkerClustersAmber , {  
    SubMarkerClustersAmber : { 
        iconCreateFunctionAmber: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
           
            console.log("amber") 
            if (childCount < 2) {
                console.log("small") 
              c += 'small-amber';
            } 
            else if (childCount < 5) {
              c += 'medium-amber';
            } 
            else {
              c += 'large-amber';
            }
           
            return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
             className: 'marker-cluster' + c, iconSize: new L.Point(10, 10) }
             )
            }      
    }  
});

window.MarkerClustersGreen = Object.assign({}, window.MarkerClustersGreen , {  
    SubMarkerClustersGreen : { 
        iconCreateFunctionGreen: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            
            console.log("green") 
            if (childCount < 2) {
                console.log("small") 
              c += 'small-green';
            } 
            else if (childCount < 5) {
              c += 'medium-green';
            } 
            else {
              c += 'large-green';
            }
           
            return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
             className: 'marker-cluster' + c, iconSize: new L.Point(10, 10) }
             )
            }      
    }  
});



window.PumpLayer = Object.assign({}, window.PumpLayer, {  
    SubPumpLayer: {  
        
        pointToLayerPump: function(feature, latlng, context) {  
            console.log(context) 

            return L.marker(latlng,  L.icon({
                iconUrl:  "assets/sewer.png", 
                
            }))

        

        
    }  
}});




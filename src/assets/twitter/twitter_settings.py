
class Settings():
    #screen size
    def __init__(self):
        ''' search variables: '''
        #search_phrases = ['Better Than Butter', 'betterthanbutter']
        self.time_limit = 1.5                           # runtime limit in hours
        self.max_tweets = 100                           # number of tweets per search (will be
                                                   # iterated over) - maximum is 100
        self.min_days_old, self.max_days_old = 0, 8          # search limits e.g., from 7 to 8
                                                   # gives current weekday from last week,
                                                   # min_days_old=0 will search from right now
        self.USA = '39.8,-95.583068847656,2500km'       # this geocode includes nearly all American
                                                   # states (and a large portion of Canada)
        self.London = '51.50291, -0.11383, 20mi'

        self.UK = '53.4123001,-3.0561416, 1mi' # based on Liverpool as center

        # Geocode
        #Returns tweets by users located within a given radius of the given latitude/longitude.
        # The location is preferentially taking from the Geotagging API, but will fall back to their Twitter profile.
        #The parameter value is specified by " latitude,longitude,radius ",
        #where radius units must be specified as either " mi " (miles) or " km " (kilometers).

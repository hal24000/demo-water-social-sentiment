#Open in Python Editor
import tweepy
import os
from tweepy import OAuthHandler
import json
import datetime as dt


from twitter_auth import AuthTwitter
from twitter_search import TweetSearch

UK_water_companies_twitterid = {'Anglian Water': '@AnglianWater', 
                                'Dŵr Cymru' : '@DwrCymru', 
                                'Welsh Water': '@DwrCymru', 
                                'Northumbrian Water': ['@NorthumbrianH2O', '@nwater_care'], 
                                'Celtic Anglian Water': '@CelticAnglian',  # irleand 
                                'Wave Utilities': '@WaveUtilitiesUK', #Anglian Water Business is now Wave
                                'Severn Trent Water': '@stwater', 
                                'Southern Water': '@SouthernWater', 
                                'South West Water' : ['@SWWHelp', '@SWWBusiness', '@SouthWestWater', '@Source4b'], 
                                'Source for Business': '@Source4b',  #' new name for ' South West Water Business
                                'Thames Water': ['@thameswater', '@ThamesCSOAlerts' ], 
                                'Thames CSO Alerts' : '@ThamesCSOAlerts', 
                                'United Utilities': ['@unitedutilities', '@uugroupplc'], 
                                'Wessex Water': ['@wessexwater', '@ukgeneco'],
                                'GENeco' : '@ukgeneco',  #part of wessex water food and liquid waste treatment and recycling to the composting of difficult to treat biodegradable material
                                'Yorkshire Water': ['@YorkshireWater', '@YWHelp'], 
                                'Hafren Dyfrdwy': '@hafrendcymru', 
                                'Affinity Water': '@AffinityWater',
                                'Albion Water': '@AlbionWater', 
                                'Bournemouth Water': ['@BmouthWater', '@BWBusinessServ' ],
                                'Bristol Water': ['@BristolWater'], 
                                'Cambridge Water Company': ['@Cambswater'], 
                                #'Cholderton and District Water Company': None,  #'Cholderton Water'
                                'Essex and Suffolk Water': '@eswater_care',
                                'Hartlepool Water': '@HartlepoolWater', # This account is no longer monitored. For all enquiries please contact Anglian Water @anglianwater
                                'Portsmouth Water': '@PortsmouthWater', 
                                'South East Water' : '@sewateruk', 
                                #'South Staffordshire Water': None, 
                                'Sutton and East Surrey Water' : '@SESWater',
                                'SESWater' :  '@SESWater',
                                #'Youlgrave Waterworks': None, 
                                }

number_of_tweets = 10

def write_tweets( timeline, filename):
            ''' Function that appends tweets to a file. '''

            json_file_root = "results_timelines"
            print(json_file_root)
            os.makedirs(json_file_root, exist_ok=True)
    

            with open(f"results_timelines/{filename}.json", 'a+') as f:
                for tweet in timeline:
                    json.dump(tweet._json, f)
                    f.write('\n')
            return print("written to file")


def main():
    ''' This is a script that continuously searches for tweets
        that were created over a given number of days. The search
        dates and search phrase can be changed below. '''

    test = AuthTwitter() #instaniate authorisation class
    api = test.load_api() #load_api() of class method
    tweets = {}

    for i in [ x for x in UK_water_companies_twitterid.values()]:
        if type(i) is str: 
            print(f"getting tweets for {i}")

            #timeline = tweepy.Cursor(api.user_timeline, 
            #                        screen_name= i , 
            #                        tweet_mode="extended").items()


            #print(timeline)

            timeline =  api.user_timeline(screen_name = i,
                                            tweet_mode="extended", 
                                            truncated = False, 
                                            count=200, 
                                            )
            tweets[i] = timeline
            write_tweets( timeline, filename = i.replace('@', ''))

        
        elif type(i) is list: 
            for j in i: 
                print(f"getting tweets for {j}")
                
                timeline =  api.user_timeline(screen_name = j,
                                            tweet_mode="extended", 
                                            truncated = False, 
                                            count=200, 
                                            )
                #print(timeline)
                tweets[j] = timeline
                write_tweets(timeline, filename = j.replace('@', ''))
        else: 
            pass 


    with open('result.json', 'w') as fp:
        json.dump(tweets, fp)



if __name__ == "__main__":
    main()



# going to do as break down due to size

#import relevant modules
import os
import sys
import json
import csv
import lxml
import pandas as pd
import numpy as np

import dash 
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State

os.getcwd()

#sys.path.insert(0,'../../../') reset to view collaborate outside src
from collaborate.file_management import CollaborateManageFiles
from collaborate.twittertools import CollaboarteTwitter, TweetPlots

from staticdata import df

from appstart import app




collabtwitter = CollaboarteTwitter()

tweetplots = TweetPlots()

#keyword_frequency_all_time = collabtwitter.get_word_frequency(df = df, 
#                                                textcol = 'tweet_text' 
#                                                )
#keyword_frequency_last_week = collabtwitter.get_word_frequency(df = df_previous_week,
#                                                 textcol = 'tweet_text' 
#                                                 )




overview = html.Div(className = 'overview-visual-contents', 
                    id = 'id-overview-visual-contents',
                        children = 
                                    [ ]
                                    

                                   
                         ) 

@app.callback( 
              [Output('id-selected_company', 'children'),
              Output('id-overview-visual-contents', 'children')],
              Input('id-df-filter-dropdown', 'value'), # companies
              Input('id-df-filter2-dropdown', 'value'), # key words
              #Input('id-radio-retweets', 'value'), # key words
              )
def filter_dataframe_by_company(company, serach_terms, ):  # retweets
        global df
        print("SCOTT EDIT 5, ", df)
        print("SCOTT EDIT 5, ", list(df))
        print("callback df: ",  set(df["search_terms"]))
        print("COMPANY : ",  company)
        if company == 'all':
                df_ = df 
        else:
                df_ = collabtwitter.filter_df_by_company(df = df, company = company.lower())

        df_previous_week = df_.loc[df_['created_at'] > (df_['created_at'].max() - pd.DateOffset(7))].copy()
        

        plots_dict = { 

                            #'fig_word_cloud' : collabtwitter.get_frequency_word_cloud(keyword_frequency = keyword_frequency_all_time ), 

                            #'fig_word_cloud' : collabtwitter.get_frequency_word_cloud(keyword_frequency = keyword_frequency_last_week ), 

                            'fig_alltime_tweet_counts' : tweetplots.get_figure_(df = df_, x = 'day', timespan = 'Since October 2021'), 

                            'fig_lastweek_tweet_counts' : tweetplots.get_figure_(df = df_previous_week, x = 'day', timespan = 'last week'),

                            'fig_sentiment_by_day_all_time' : tweetplots.plot_polarity_histogram_by_day(df = df_, timespan = "since start"),

                            'fig_sentiment_by_day_last_week' : tweetplots.plot_polarity_histogram_by_day(df = df_previous_week, timespan = "last week"),

                            'fig_sentiment_pie_since_start' : tweetplots.plot_sentiment_pie(df_, timeperiod = 'since scraping started - Oct 2021'), 

                            'fig_sentiment_pie_last_week' : tweetplots.plot_sentiment_pie(df_previous_week, timeperiod = 'last week'), 

                            }

        
        plots = [dcc.Graph(figure = v, id = f'id-{k}' ) for k, v in plots_dict.items()]

        
        
        #retweets_radio_button = dcc.RadioItems( id= 'id-radio-retweets', 
        #                                        options=[
        #                                                {'label': 'Include Retweets', 'value': 'rt'},
        #                                                {'label': 'Exclude Retweets', 'value': 'source_tweets'},
        #                                        ],
        #                                        #value='source_tweet'
        #                                        )

        #if retweets == 'source_tweet':
        #        df_tweets = df_
        #else:
        #        df_tweets = None

        tweets = collabtwitter.get_tweet_text_dash_html(df = df_.reset_index()) #.loc[df['tweet_text'].str.contains('sewage')].reset_index())


        #tweetselect = [html.Div(retweets_radio_button)]
        #tweets.extend(tweetselect)
        plots.extend(tweets)

        return html.H2(f"Information for {company}"), plots





import sys 
import folium 

import dash_html_components as html
import dash_core_components as dcc

#sys.path.insert(0,'../../../') reset to view collaborate outside src
from collaborate.mappingtools import CollaboarteFoliumMaps


# Create map centered on Bristol Water Area
m = CollaboarteFoliumMaps.BristolWater()
# Add Bristol Water areas (dervied from shp file found from ofwat link to bristol site)
# see https://www.ofwat.gov.uk/regulated-companies/markets/water-bidding-market/water-resources-market-information/
polygon_filepath_dict = { "Bristol Water PLC" : "assets/geojsons/bristol-water-plc.geojson", 
                         "Non SSE" : "assets/geojsons/BW Supply Area_ NOT SSE.geojson", 
                         "SSE" : "assets/geojsons/Scottish and Southern Energy.geojson"
                        }
m.add_polygons(polygon_filepath_dict = polygon_filepath_dict) 

# An example issue url and location - tweet in this instance
# Add issue as point with html popup 
url = "https://twitter.com/octopusgirl7/status/1445769542601363464"
tweet = "@bristolwater burst pipe on unity street, bs2. any ideas on when it will be fixed? tons of people without water here."
html_string = f'''<H1> Twitter Issue </H1>
<div> {tweet} </div>
<a href={url} target="_blank" rel="noopener noreferrer"> See Tweet</a> '''
location = [51.4550008,-2.5827324]
icon = folium.Icon(color='red')
m.add_point(html_string = html_string, 
           location = location, 
           icon = icon)
# add layer control 
m.add_layer_control()
# view map
m.get_map()

folium_map = html.Div( className = 'folium-output', 
                    children = [html.Iframe( 
                                            srcDoc= m.get_raw_html() )] 
                    )
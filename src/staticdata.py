import sys
import pandas as pd

#sys.path.insert(0,'../../../') reset to view collaborate outside src
from collaborate.file_management import CollaborateManageFiles
from collaborate.twittertools import CollaboarteTwitter

collaboratefiles = CollaborateManageFiles()
PATH = "assets/twitter"
tweet_files = collaboratefiles.get_files_in_folder( PATH = PATH,  recursive = True, ext_ = 'json' )

#print("SCOTT EDIT 2, tweet files ", tweet_files)
#print("SCOTT EDIT 2, path  ", PATH )
#import os
#print("SCOTT EDIT 2, cwd  ", os.getcwd() )
#print("SCOTT EDIT 2, cwd ", [f for f in os.listdir('.')])
collabtwitter = CollaboarteTwitter()

# generate datafarme from list of tweet files 
df = collabtwitter.build_tweet_df(tweet_files = tweet_files)
# get sentiment on raw tweet 
df = collabtwitter.get_text_sentiment(df = df, col = 'tweet_text', identifier = 'raw')
# make words lower case 
df = collabtwitter.get_dataframe_lower(df = df, cols = ['tweet_text', 'search_terms'])
# filter by company of interest 
# NOTE REQUIRES LOWERCASE!!!

